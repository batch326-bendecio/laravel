@extends('layouts.app')

@section('tabName')
	Edit {{$post->title}}
@endsection

@section('content')

	<form class = "col-6 bg-secondary p-5 mx-auto rounded-4" method = "POST" action="/posts/{{$post->id}}/edit">

		@csrf
		<div class="form-group">
			<label for = "title">Title:</label>
			<input type="text" name="title" class = "form-control" id = "title" value="{{$post->title}}" />
		</div>

		<div class = "form-group">
			<label for = "content">Content:</label>
			<textarea class = "form-control" id = "content" name="content" rows =3>{{$post->body}}</textarea>
		</div>

		<div class = "mt-3">
			<button class = "btn btn-primary">Save</button>
		</div>

	</form>




@endsection