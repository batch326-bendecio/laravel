@extends('layouts.app')

@section('tabName')
	{{$post->title}}
@endsection
@section('content')
	
	<div class="card mx-auto">
		<div class="card-body">
			<h2 class="card-title">{{$post->title}}</h2>
			<p class="card-subtitle text-muted">Author: {{$post->user->name}}</p>
			<p class="card-subtitle text-muted mb-3"> Created at: {{$post->created_at}}</p>
			<h5 class="card-text text-muted mb-5">{{$post->body}}</h5>
			<p class="card-text text-muted mb-4">Likes: {{count($post->likes)}} | Comments: {{count($post->comments)}}</p>

			@if(Auth::user())
				@if(Auth::id() != $post->user_id)
				<form class="d-inline" method="POST" action="/posts/{{$post->id}}/like">
					@method('PUT')
					@csrf
					@if($post->likes->contains('user_id', Auth::id()))
					<button class="btn btn-danger">Unlike</button>

					@else
					<button class="btn btn-success">Like</button>

					@endif
				</form>

				@endif

					<button type="button" class="btn btn-primary" data-bs-toggle="modal" data-bs-target="#exampleModal">
					  Comment
					</button>

					<div class="modal fade" id="exampleModal" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
					  <div class="modal-dialog">
					      <div class="modal-content">
					        <div class="modal-header">
					          <h1 class="modal-title fs-5 text-muted" id="exampleModalLabel">Leave a comment</h1>
					          <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
					        </div>

					        <form method='POST' action="/posts/{{$post->id}}/comment">

					    	  	@csrf
					        	<div class="modal-body">
					        	  	<label for = "content" class="text-muted mb-1">Content:</label>
					        	  	<textarea class = "form-control" id = "content" name="content" rows =3></textarea>
					        	</div>
					        	<div class="modal-footer">
					        	  <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Close</button>
					        	  <button type="submit" class="btn btn-primary">Comment</button>
					        	</div>
					        </form>
					      </div>
					  </div>
					</div>

			
			@endif
			<br/>

			<a href="/posts" class="btn btn-primary mt-2">View All Post</a>
		</div>
	</div>


	<h4 class="text-muted mt-5">Comments:</h4>
	@if(count($post->comments)>0)
		@foreach($post->comments as $comment)
			<div class="card mt-2">
				<div class="card-body">
					<p class="card-text">{{$comment->content}}</p>
					<p class="text-muted mt-5" style="line-height: 2px; margin-left: 54rem; font-size: 0.8rem;">commented by: {{$comment->user->name}}</p>
					<p class="text-muted mb-1" style="line-height: 2px; margin-left: 54rem; font-size: 0.8rem;">commented on: {{$comment->created_at}}</p>
				</div>
			</div>
		@endforeach
	@else

	<div class="text-center text-muted">
		<h2>There are no posts to show</h2>
	</div>
	@endif




@endsection