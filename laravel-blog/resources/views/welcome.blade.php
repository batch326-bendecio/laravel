@extends('layouts.app')

@section('tabName')
    Welcome
@endsection

@section('content')

<div class="d-flex justify-content-center">
    <img src="https://cdn.freebiesupply.com/logos/large/2x/laravel-1-logo-png-transparent.png" class="img-fluid w-50">
</div>

    <h4 class="text-center text-muted mt-3 mb-3">Featured Posts:</h4>

    @if(count($featuredPosts)>0)
        @foreach($featuredPosts as $post)
            <div class="card text-center mx-auto mt-2 ">
                <div class="card-body">
                    <h4 class="card-title mb-3"><a href="/posts/{{$post->id}}">{{$post->title}}</a></h4>
                    <p class="card-text mb-3">Author: {{$post->user->name}}</p>
                </div>
            </div>
        @endforeach

    @else
        <div>
            <h2 class="text-center mt-5 mb-5">There are no posts yet</h2>
        </div>
    @endif
@endsection