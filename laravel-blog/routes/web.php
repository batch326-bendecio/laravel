<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\PostController;


/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', [PostController::class, 'showFeaturedPosts']);

Auth::routes();

Route::get('/home', [App\Http\Controllers\HomeController::class, 'index'])->name('home');

// This route is for logging out.
Route::get('/logout', [PostController::class, 'logout']);

// This route is for creation of a new post.
Route::get('/posts/create', [PostController::class, 'createPost']);

// This route is for saving the post on our database.
Route::post('/posts', [PostController::class, 'savePost']);

// This route is for the list of post on our database.
Route::get('/posts', [PostController::class, 'showPosts']);

// Define a route that will return a view containing only the authenticated user's post
Route::get('/myPosts', [PostController::class, 'myPosts']);

// define a route wherein a view showing a specific post with matching URL parameter ID will be returned to the user
Route::get('/posts/{id}', [PostController::class, 'show']);

// This is route to show the post that wanted to update
Route::get('/posts/{id}/edit', [PostController::class, 'updatePost']);

Route::post('/posts/{id}/edit', [PostController::class, 'saveUpdatedPost']);

Route::get('/posts/{id}/archive', [PostController::class, 'archivePost']);

Route::put('/posts/{id}/like', [PostController::class, 'like']);

Route::post('/posts/{id}/comment', [PostController::class, 'comment']);