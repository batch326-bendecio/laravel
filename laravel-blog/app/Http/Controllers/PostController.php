<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

// access the authenticated user via the Auth facade
use Illuminate\Support\Facades\Auth;

use App\Models\Post;
use App\Models\PostLike;
use App\Models\PostComment;

class PostController extends Controller
{   

    // Controller is for the user logout
    public function logout(){
        // it will logout the currently logged in user.
        Auth::logout();

        return redirect('/login');
    }

    // Action to return a view containing a form for a blog post creation.
    public function createPost(){
        return view('posts.create');
    }

    public function savePost(Request $request){
       // to check whether there is an authenticated user

       if(Auth::user()){
             // instantiate a new Post Object from the Post method and then save it in $post variable
             $post = new Post;

            // define the properties of the $post object using the received form data
            $post->title=$request->input('title');
            $post->body=$request->input('content');

            // this will get the id of the authenticated user and set it as the foreign jet user_id of the new post.
            $post->user_id=(Auth::user()->id);

            // save the post object in our Post table.
            $post->save();

            return redirect('/posts');

        } else {

          return redirect('/login');

        }

    }

    // controller will return all the blog posts
    public function showPosts(){
        // Retrieve all records from the Post table
        $posts = Post::all();

        // Create an array to store active posts
        $activePosts = [];

        // Loop through each post to filter active posts
        foreach ($posts as $post) {
            if ($post->isActive == 1) {
                $activePosts[] = $post;
            }
        }

        // Pass the active posts to the view
        return view('posts.showPosts')->with('posts', $activePosts);
    }

    // Show featured posts
    public function showFeaturedPosts(){

        $featuredPosts = Post::inRandomOrder()->take(3)->get();

        $activeFeaturedPosts = [];

        // Loop through each post to filter active posts
        foreach ($featuredPosts as $post) {
            if ($post->isActive == 1) {
                $activeFeaturedPosts[] = $post;
            }
        }


        return view('welcome')->with('featuredPosts', $activeFeaturedPosts);

    }

    // action for showing only the posts authored by authenticated user
    public function myPosts(){
        if(Auth::user()){

            $posts = Auth::user()->posts;

            return view('posts.showPosts')->with('posts', $posts);

        } else {

            return redirect('/login');

        }
    }

    // action that will return a view showing a specific post using the URL parameter $id to query for the database entry to be shown
    public function show($id){

        $post = Post::find($id);

        return view('posts.show')->with('post', $post);
    }

    public function updatePost($id){

        $post = Post::find($id);

        return view('posts.edit')->with('post', $post);
    }

    public function saveUpdatedPost(Request $request, $id){

        $post = Post::find($id);

            $post->title=$request->input('title');
            $post->body=$request->input('content');

            $post->save();

            return redirect('/posts');

    }

    public function archivePost($id){

        $post = Post::find($id);

        if($post){

            $post->isActive = $post->isActive == 1 ? 0 : 1;

            $post->save();

            return redirect('/posts');

        } else {

            return redirect('/posts')->with('error', 'Post not found');

        }
    }

    public function like($id){

        $post = Post::find($id);

        if($post->user_id != Auth::user()->id){

            if($post->likes->contains('user_id', Auth::user()->id)){

                PostLike::where('post_id', $post->id)->where('user_id', Auth::user()->id)->delete();

            } else {
                // create a new like record to like this post
                // instantiate a new PostLke object from the PostLike model

                $postLike = new PostLike();

                $postLike->post_id = $post->id;
                $postLike->user_id = Auth::user()->id;

                $postLike->save();

            }

        }

        return redirect("/posts/$id");
    }

    public function comment(Request $request, $id){

        $post = Post::find($id);

        if(Auth::user()){

            $postComment = new PostComment;

            $postComment->content = $request->input('content');
            $postComment->user_id = Auth::user()->id;
            $postComment->post_id = $post->id;

            $postComment->save();

            return redirect("/posts/$id");

        } else {

            return redirect('/login');

        }
    }
}
