<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class PostLike extends Model
{
    public function post(){
        $this->belongsTo('App\Models\Post');
    }

    public function user(){
        $this->belongsTo('App\Models\User');
    }
}
